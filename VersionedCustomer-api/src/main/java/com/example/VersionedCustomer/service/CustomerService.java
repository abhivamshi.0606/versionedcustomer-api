package com.example.VersionedCustomer.service;

import com.example.VersionedCustomer.models.Customer;
import com.example.VersionedCustomer.repository.CustomerRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class CustomerService {

    @Autowired
    CustomerRepository customerRepository;

    public Customer getCustomerById(long customerId) {
        return customerRepository.findById(customerId).get();
    }

    public Customer getCustomerByIdAndVersion(long customerId, int version) {
        return customerRepository.findById(customerId).get();
    }

    public Customer createNewCustomer(Customer customer) {

        Optional<Customer> customerExisting = customerRepository.findByFirstNameAndLastName(customer.getFirstName(), customer.getLastName());

        customerExisting.ifPresent(customer1 -> customer.setId(customer1.getId()));
        System.out.println("customer = " + customer);
        customer.setCreatedAt(LocalDateTime.now());
        return customerRepository.save(customer);
    }
}
