package com.example.VersionedCustomer.controllers;

import com.example.VersionedCustomer.constants.Routes;
import com.example.VersionedCustomer.models.Customer;
import com.example.VersionedCustomer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class CustomerController {

    @Autowired
    CustomerService customerService;

    @GetMapping(value = Routes.GET_CUSTOMER_BY_ID)
    private Customer getCustomer(@PathVariable("customerId") long customerId)
    {
        return customerService.getCustomerById(customerId);
    }

    @GetMapping(value = Routes.GET_CUSTOMER_BY_ID_AND_VERSION)
    private Customer getCustomer(@PathVariable("customerId") long customerId, @PathVariable("version") int version)
    {
        return customerService.getCustomerByIdAndVersion(customerId, version);
    }

    @PostMapping(value = Routes.CREATE_CUSTOMER)
    private long saveBid(@RequestBody Customer customer) {
        Customer customerSaved = customerService.createNewCustomer(customer);
        return customerSaved.getId();
    }
}
