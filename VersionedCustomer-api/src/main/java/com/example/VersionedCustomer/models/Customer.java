package com.example.VersionedCustomer.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table
@Getter
@Setter
@ToString
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    private String email;

    @Column
    private int age;

    @CreatedDate
    private LocalDateTime createdAt;

    @Version
    @Column(insertable = false, updatable = false)
    @org.hibernate.annotations.Generated(value = GenerationTime.ALWAYS)
    private Long version;
}
