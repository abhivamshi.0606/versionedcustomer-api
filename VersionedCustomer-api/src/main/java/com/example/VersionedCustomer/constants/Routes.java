package com.example.VersionedCustomer.constants;

public class Routes {
    public static final String GET_CUSTOMER_BY_ID = "/customers/{customerId}";
    public static final String CREATE_CUSTOMER = "/customers";
    public static final String GET_CUSTOMER_BY_ID_AND_VERSION = "/customers/{customerId}/version/{version}";
}
