package com.example.VersionedCustomer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VersionedCustomerApplication {

	public static void main(String[] args) {
		SpringApplication.run(VersionedCustomerApplication.class, args);
	}

}
